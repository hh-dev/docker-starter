const settings = {
    app: {
        environment: process.env.NODE_ENV,
        debug: process.env.DEBUG === 'true'
    },
    api: {
        port: process.env.PORT
    }
};

module.exports = settings;
