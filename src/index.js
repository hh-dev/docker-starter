const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const handleHealthy = require('./views/routes/healthy');
const handleNotFound = require('./views/routes/not-found');
const requestLogger = require('./views/middleware/request-logger');
const configUtils = require('./utils/config');
const logger = require('./utils/logger');

const app = express();

// Init middleware
app.use(requestLogger());
app.use(cors());
app.use(bodyParser.json({ type: 'application/json' }));

// Endpoints
app.use('/healthy', handleHealthy);

// Handle unknown routes
app.use('/*', handleNotFound);

const port = configUtils.getApiPort();
app.listen(port, () => logger.info(`Application started on port: ${port}`));
