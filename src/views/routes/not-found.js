const router = require('express').Router();
const logger = require('../../utils/logger');
const responseManager = require('../../utils/response-manager');

router.get('/', async (req, res) => {
    try {
        responseManager.handleError(res, 404);
    } catch (error) {
        logger.error(error);
        responseManager.handleError(res, 500);
    }
});

module.exports = router;
