const fs = require('fs');
const path = require('path');
const gulp = require('gulp');
const nodemon = require('gulp-nodemon');

// If dev environment, import appropriate environment variables
const envFile = path.join(__dirname, '.env');
if (fs.existsSync(envFile)) {
    // eslint-disable-next-line global-require
    require('dotenv').config({ path: envFile });
}

gulp.task('development', () => {
    return nodemon({ script: 'src/index.js', watch: ['src/**/*.js'] })
        .once('quit', () => {
            process.exit();
        });
});
