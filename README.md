# Docker Starter

## System requirements
- Docker
    - Windows: https://store.docker.com/editions/community/docker-ce-desktop-windows
    - Mac: https://store.docker.com/editions/community/docker-ce-desktop-mac
- Node.js 10.x
- NPM 6.x

## Project setup
- Clone repository: `git clone git@bitbucket.org:hh-dev/docker-starter.git`
- Install dependencies: `npm i`

#### Environment variables
In development, create a file called _.env_ in the project's root directory and add the following variables:

| Variable | Required | Default     | Description                                       |
|----------|----------|-------------|---------------------------------------------------|
| DEBUG    | No       | false       | Enable debug logs                                 |
| NODE_ENV | No       | development | Node environment <production, development, test   |
| PORT     | Yes      |             | Port on which to serve the application's API      |

The file will contain the environment variables to be initialized for development. Add environment-specific variables on new lines in the form of `<NAME>=<VALUE>`. See [dotenv](https://www.npmjs.com/package/dotenv) for usage.


## Development
In the _package.json_ file, a convenience script, which uses Gulp has been included to start the app. Simply run 
```
npm start
```

The app should start on the _PORT_ specified in your _.env_ file.

## Docker
Will be covered in training session.
